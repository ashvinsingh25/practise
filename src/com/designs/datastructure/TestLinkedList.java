package com.designs.datastructure;

public class TestLinkedList {
public  static void main(String ... arg){
    LinkedList linkedList = new LinkedList();

    for(int i = 0; i<= 10;i++)
    {
        LinkedList.insert(linkedList,120+1*i);
    }

    System.out.println("Linked List data  :: ");
    LinkedList.displayNodes(linkedList);
    linkedList = LinkedList.deleteNode(linkedList,120);
    linkedList = LinkedList.deleteNode(linkedList,130);
    linkedList = LinkedList.deleteNode(linkedList,131);

    System.out.println("Linked List data  present now :: ");
    LinkedList.displayNodes(linkedList);

    linkedList  = LinkedList.removeFromIndex(linkedList,9);
    linkedList = LinkedList.removeFromIndex(linkedList,0);
    linkedList = LinkedList .removeFromIndex(linkedList,9);

    System.out.println("Linked List data  present now :: ");
    LinkedList.displayNodes(linkedList);



}

}
