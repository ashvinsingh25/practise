package com.designs.datastructure;

public class LinkedList {
    private Node head ;

    private  static class Node {
        int data;
        Node next;
        private Node(int data){
            this.data = data;
            this.next = null;
        }
    }

    public static LinkedList insert(LinkedList linkedList , int data){
        Node newNode = new Node(data);
        if(linkedList.head == null)
        {
            linkedList.head = newNode;
            return linkedList;
        }
        else {
            Node lastNode = linkedList.head;
            while (lastNode.next != null){
                lastNode = lastNode.next;
            }
            lastNode.next=newNode;
        }
        return linkedList;
    }

    public static void displayNodes(LinkedList linkedList){
        Node currentNode = linkedList.head;
        if (currentNode == null){
            System.out.println("You Have Passed Empty Linked List");
         }
        else {
            int count = 0;
            while (currentNode != null){
                System.out.println("index :"+ count +" value : "+currentNode.data);
                currentNode = currentNode.next;
                count ++;
            }
        }
    }

    public static LinkedList deleteNode (LinkedList linkedList,int data){
        //if head need to be removed
        Node currentNode = linkedList.head;
        Node previousNode = null;
        if(currentNode != null && currentNode.data == data){
            currentNode = currentNode.next;
        }
        else {

            while (currentNode != null && currentNode.data != data){
                previousNode = currentNode;
                currentNode = currentNode.next;
            }
            if (currentNode != null){
                previousNode.next = currentNode.next;

                System.out.println("Key Present and Removed "+currentNode.data);
            }else{
                System.out.println("Key Not Present "+data);
            }

        }
        return linkedList;
    }

    public  static  LinkedList removeFromIndex (final LinkedList linkedList, final int index){
         if(index < 0 ){
             System.out.println("invalid index ");
         return linkedList;
         }
         Node currentNode = linkedList.head;
         if (index == 0 && currentNode != null){
             System.out.println("head Removed ");
             linkedList.head = currentNode.next;
             return linkedList;
         }

         Node previousNode = null;
         int count = 0 ;
         while (currentNode != null){
             if(index == count){
                 previousNode.next = currentNode.next;
                 System.out.println("node removed from index"+ index);
                 break;
             }else {
                 count ++;
                 previousNode = currentNode;
                 currentNode = currentNode.next;
             }

         }
         if (currentNode == null){
             System.out.println(" no node removed from index"+ index);
         }

         return linkedList;
    }
}
