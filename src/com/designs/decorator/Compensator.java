package com.designs.decorator;

public class Compensator extends GunAccessories {
    Gun gun;
    Double cost;
    public Compensator(Gun gun) {
        this.cost = 1000.0;
        this.gun = gun;
    }

    public Double getCost() {
        return this.cost + this.gun.getCost();
    }
}
