package com.designs.decorator;

public class Scope extends  GunAccessories {

    Gun gun;
    Double cost;
    public Scope(Gun gun) {
        this.cost = 500.0;
        this.gun = gun;
    }

    public Double getCost() {

        return this.cost+ this.gun.getCost();
    }


}
