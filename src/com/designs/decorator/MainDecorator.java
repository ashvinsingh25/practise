package com.designs.decorator;

public class MainDecorator {

    public static void main(String[] args) {
        System.out.println("Hello ");

        Gun scarl = new Scarl();
        scarl = new Compensator(scarl);
        scarl = new Suppressor(scarl);
        scarl = new Scope(scarl);
        System.out.println("Price is " + scarl.getCost());
    }


}
