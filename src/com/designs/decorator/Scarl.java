package com.designs.decorator;


public class Scarl extends Gun {
    Double cost;

    public Scarl() {
        this.cost = 5000.0;
    }

    @Override
    public Double getCost() {
        return this.cost;
    }
}
