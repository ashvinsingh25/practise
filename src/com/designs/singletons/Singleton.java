package com.designs.singletons;

import java.io.Serializable;

public class Singleton implements   Cloneable,Serializable  {

    public  static  int count = 0;
    private static Singleton singletone;

    public static  int value = 90;
    private Singleton (){
        if(singletone!=null){
            throw new InstantiationError("Already there");
        }
        count = ++count;
    }

    public static Singleton getInstance() {
        if (singletone == null) {
            synchronized (Singleton.class) {
                if (singletone == null) {
                    singletone = new Singleton();
                }
            }
        }
        return singletone;
    }
    @Override
    public Object clone() throws CloneNotSupportedException  {
        return singletone;
    }

    private Object readResolve(){
        return singletone;
    }
}
