import com.designs.singletons.Singleton;

import java.io.*;
import java.lang.reflect.Constructor;

public class Main {

    public static void main(String[] args) throws Exception {

        Singleton singleton1 = Singleton.getInstance();
        System.out.println(singleton1.hashCode());
        Singleton singleton2 = null;

      /*  Singleton singleton2 = null;
        Constructor constructors []  = Singleton.class.getDeclaredConstructors();
        Constructor constructor  = constructors[0];
        constructor.setAccessible(true);
        singleton2 = (Singleton)constructor.newInstance();*/

       /* ObjectOutput out = new ObjectOutputStream(new FileOutputStream("file.text"));
        out.writeObject(singleton1);
        out.close();
        ObjectInput in = new ObjectInputStream(new FileInputStream("file.text"));
        Singleton singleton2 = (Singleton) in.readObject();
        in.close();*/

        /*Singleton singleton2 = (Singleton) singleton1.clone();*/
       /* ObjectOutput out = new ObjectOutputStream(new FileOutputStream("file.text"));
        out.writeObject(singleton1);
        out.close();
        ObjectInput in = new ObjectInputStream(new FileInputStream("file.text"));
         singleton2 = (Singleton) in.readObject();
        in.close();*/

        Constructor constructors []  = Singleton.class.getDeclaredConstructors();
        Constructor constructor  = constructors[0];
        constructor.setAccessible(true);
        singleton2 = (Singleton)constructor.newInstance();
        System.out.println(singleton2.hashCode());
    }
}
